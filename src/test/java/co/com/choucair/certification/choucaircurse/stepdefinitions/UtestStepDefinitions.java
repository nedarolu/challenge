package co.com.choucair.certification.choucaircurse.stepdefinitions;



import co.com.choucair.certification.chourcaircurse.tasks.OpenUp;
import co.com.choucair.certification.chourcaircurse.tasks.Signup;

import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;


public class UtestStepDefinitions {

    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than Michael wants to learn automation at the academy Choucair$")
    public void thanMichaelWantsToLearnAutomationAtTheAcademyChoucair() throws Exception {
        OnStage.theActorCalled( "Michael").wasAbleTo(OpenUp.thePage(),Signup.the() );


    }

    @When("^he search for the course  on the choucair academy platform$")
    public void heSearchForTheCourseMeansAutomationBancolombiaOnTheChoucairAcademyPlatform() throws Exception {

    }

    @Then("^he finds the course called resources $")
    public void heFindsTheCourseCalledResourcesMeansAutomationBancolombia() throws Exception {


    }
}
