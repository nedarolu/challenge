package co.com.choucair.certification.chourcaircurse.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class SignupUtest extends PageObject {



    public static final Target INPUT_FIRSNAME= Target.the("Where we write the first name")
            .located(By.id("firstName"));
    public static final Target INPUT_LASTNAME= Target.the("Where we write the last name")
            .located(By.id("lastName"));
    public static final Target INPUT_EMAIL= Target.the("Where we write the email")
            .located(By.id("email"));
    public static final Target SELECT_MONTH= Target.the("Where we select the month")
            .located(By.id("birthMonth"));
    public static final Target SELECT_DAY= Target.the("Where we select the Day")
            .located(By.id("birthDay"));
    public static final Target SELECT_YEAR= Target.the("Where we select the Year")
            .located(By.id("birthYear"));
    public static final Target ENTER_BUTTON_LOCATION= Target.the("Click to next location")
            .located(By.xpath("//a[contains(@class,'btn btn-blue')]"));
    public static final Target INPUT_CITY_LOCATION= Target.the("Where we write the city")
            .located(By.id("city"));
    public static final Target INPUT_ZIP_LOCATION= Target.the("Where we write the Postal Code")
            .located(By.id("zip"));
    public static final Target ENTER_BUTTON_DEVICES= Target.the("Click to next devices")
            .located(By.xpath("//a[contains(@class,'btn btn-blue pull-right')]"));








}
