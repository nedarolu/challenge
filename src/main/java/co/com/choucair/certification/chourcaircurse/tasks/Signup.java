package co.com.choucair.certification.chourcaircurse.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import co.com.choucair.certification.chourcaircurse.userinterface.SearchUtest;
import co.com.choucair.certification.chourcaircurse.userinterface.SignupUtest;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByValueFromBy;

public class Signup implements Task  {

    public static Signup the() {
        return Tasks.instrumented(Signup.class);
    }

    @Override
    public  <T extends Actor> void performAs(T actor){
        actor.attemptsTo(Click.on(SearchUtest.BUTTON_SINGUP),
                Enter.theValue("Francisco").into(SignupUtest.INPUT_FIRSNAME),
                Enter.theValue("Santander").into(SignupUtest.INPUT_LASTNAME),
                Enter.theValue("francisco@gmail.com").into(SignupUtest.INPUT_EMAIL),
                SelectFromOptions.byValue("number:11").from(SignupUtest.SELECT_MONTH),
                SelectFromOptions.byValue("number:8").from(SignupUtest.SELECT_DAY),
                SelectFromOptions.byValue("number:1992").from(SignupUtest.SELECT_YEAR),
                Click.on(SignupUtest.ENTER_BUTTON_LOCATION),
                 Enter.theValue("Fusagasugá").into(SignupUtest.INPUT_CITY_LOCATION),
                Click.on(SignupUtest.INPUT_CITY_LOCATION),

                Enter.theValue("111311").into(SignupUtest.INPUT_ZIP_LOCATION),
                Click.on(SignupUtest.ENTER_BUTTON_DEVICES)








                );







    }
}
